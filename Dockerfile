FROM python:latest
LABEL maintainer='andika'

WORKDIR '/usr/app/src'
COPY app.py ./

EXPOSE 8080
CMD ['python3','-m','flask','run']